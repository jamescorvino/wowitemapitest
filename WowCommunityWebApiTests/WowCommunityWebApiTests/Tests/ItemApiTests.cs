﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WowCommunityWebApiTests.Tests
{
    /// <summary>
    /// Community Web Tests for Items API
    /// </summary>
    [TestClass]
    public class ItemApiTests
    {
        private const int WeaponItemId = 18803;
        private const int ArmorItemId = 76749;
        private const int SocketableItemId = 76749;

        [TestMethod]
        public void Item_Api_Response_With_Correct_Item()
        {
            var itemApi = new ItemApi(ArmorItemId);

            itemApi.Call();

            if (itemApi.ResponseMessage.IsSuccessStatusCode)
            {
                var item = itemApi.Model;

                Assert.IsTrue(item.Id == ArmorItemId, "Call returned unexpected Id of {0}!", item.Id);
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Weapon_Item_Contains_Weapon_Info()
        {
            var itemApi = new ItemApi(WeaponItemId);

            itemApi.Call();

            if (itemApi.ResponseMessage.IsSuccessStatusCode)
            {
                var item = itemApi.Model;

                Assert.IsTrue(item.WeaponInfo != null, "Item {0} is a weapon but does not contain Weapon Info!", WeaponItemId);
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Armor_Item_Is_Equippable()
        {
            var itemApi = new ItemApi(ArmorItemId);

            itemApi.Call();

            if (itemApi.ResponseMessage.IsSuccessStatusCode)
            {
                var item = itemApi.Model;
                Assert.IsTrue(item.Equippable, "Item {0} is armor but is not Equippable!", ArmorItemId);
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Item_Contains_DisplayInfoId()
        {
            var itemApi = new ItemApi(ArmorItemId);

            itemApi.Call();

            if (itemApi.ResponseMessage.IsSuccessStatusCode)
            {
                var item = itemApi.Model;

                Assert.IsTrue(item.DisplayInfoId > 0, "Item {0} does not contain a DisplayInfoId!", ArmorItemId);
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Item_Does_Not_Have_A_Duplicate_Stat_Bonus()
        {
            var itemApi = new ItemApi(WeaponItemId);

            itemApi.Call();

            if (itemApi.ResponseMessage.IsSuccessStatusCode)
            {
                var item = itemApi.Model;

                var statCount = item.BonusStats.GroupBy(s => s.Stat);

                foreach (var stat in statCount)
                {
                    Assert.IsTrue(stat.Count() == 1, "Stat {0} appeared {1} times in BonusStats!", stat, stat.Count());
                }
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Item_With_Sockets_Contains_SocketInfo()
        {
            var itemApi = new ItemApi(SocketableItemId);

            itemApi.Call();

            if (itemApi.ResponseMessage.IsSuccessStatusCode)
            {
                var item = itemApi.Model;

                if (item.HasSockets)
                {
                    Assert.IsTrue(item.SocketInfo != null, "Null SocketInfo for Item {0} with HasSocket value of true!", SocketableItemId);
                }
                else
                {
                    Assert.Fail("Socktable Item {0} contains HasSocket value of false!", SocketableItemId);
                }
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemApi.ResponseMessage.StatusCode);
            }
        }
    }
}
