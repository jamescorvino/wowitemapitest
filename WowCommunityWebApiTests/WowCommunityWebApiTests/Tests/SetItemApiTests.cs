﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WowCommunityWebApiTests.Tests
{
    /// <summary>
    /// Community Web Tests for Set Items API
    /// </summary>
    [TestClass]
    public class SetItemApiTests
    {
        private const int SetId = 1060;

        [TestMethod]
        public void Set_Api_Response_With_Correct_Set()
        {
            var itemSetApi = new ItemSetApi(SetId);

            itemSetApi.Call();

            if (itemSetApi.ResponseMessage.IsSuccessStatusCode)
            {
                var itemSet = itemSetApi.Model;

                Assert.IsTrue(itemSet.Id == SetId, "Call returned unexpected Id of {0}!", itemSet.Id);
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemSetApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Item_Set_Ids_Match_Set_Id_For_Individual_Items()
        {
            var itemSetApi = new ItemSetApi(SetId);

            itemSetApi.Call();

            if (itemSetApi.ResponseMessage.IsSuccessStatusCode)
            {
                var itemSet = itemSetApi.Model;

                foreach (var id in itemSet.Items)
                {
                    var itemApi = new ItemApi(id);

                    itemApi.Call();

                    if (itemApi.ResponseMessage.IsSuccessStatusCode)
                    {
                        var itemId = itemApi.Model;

                        Assert.AreEqual(SetId, itemSet.Id, "Item {0} in Set {1} does not return the same set!", id, SetId);
                    }
                    else
                    {
                        Assert.Fail("Call returned Status Code {0}!", itemSetApi.ResponseMessage.StatusCode);
                    }
                }
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemSetApi.ResponseMessage.StatusCode);
            }
        }

        [TestMethod]
        public void Set_Contain_A_Set_Bonus()
        {
            var itemSetApi = new ItemSetApi(SetId);

            itemSetApi.Call();

            if (itemSetApi.ResponseMessage.IsSuccessStatusCode)
            {
                var itemSet = itemSetApi.Model;

                var setBonuses = itemSet.SetBonuses;

                foreach (var bonus in setBonuses)
                {
                    Assert.IsTrue(bonus.Threshold > 0);

                    Assert.IsTrue(!string.IsNullOrEmpty(bonus.Description));
                }
            }
            else
            {
                Assert.Fail("Call returned Status Code {0}!", itemSetApi.ResponseMessage.StatusCode);
            }
        }
    }
}
