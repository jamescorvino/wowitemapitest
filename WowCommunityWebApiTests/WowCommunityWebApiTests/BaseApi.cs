﻿
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Net.Http;

namespace WowCommunityWebApiTests
{
    public class BaseApi<T>
    {
        private readonly int _id;
        private readonly HttpClient _client;
        private const string BaseUri = "http://us.battle.net/api/wow";

        public BaseApi(int id)
        {
            _id = id;
            _client = new HttpClient();
        }

        public virtual string AccessIdentifier { get; set; }
        public HttpResponseMessage ResponseMessage { get; set; }
        public T Model { get; set; }

        public void Call()
        {
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(string.Format("{0}/{1}/{2}", BaseUri, AccessIdentifier, _id))
            };

            ResponseMessage = _client.SendAsync(request).Result;

            if (!ResponseMessage.IsSuccessStatusCode) return;

            var responseBody = ResponseMessage.Content.ReadAsStringAsync().Result.ToString(CultureInfo.InvariantCulture);

            Model = JsonConvert.DeserializeObject<T>(responseBody);
        }
    }
}
