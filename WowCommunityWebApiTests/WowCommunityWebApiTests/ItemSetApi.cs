﻿
using WowCommunityWebApiTests.Models;

namespace WowCommunityWebApiTests
{
    public class ItemSetApi : BaseApi<ItemSet>
    {
        public ItemSetApi(int itemId) : base(itemId)
        {
            AccessIdentifier = "item/set";
        }

        public override sealed string AccessIdentifier { get; set; }
    }
}
