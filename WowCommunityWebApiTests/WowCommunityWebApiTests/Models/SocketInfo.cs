﻿
namespace WowCommunityWebApiTests.Models
{
    public class SocketInfo
    {
        public Sockets[] Sockets { get; set; }
        public string SocketBonus { get; set; }
    }
}
