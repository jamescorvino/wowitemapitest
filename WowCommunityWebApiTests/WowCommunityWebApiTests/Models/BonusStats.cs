﻿
namespace WowCommunityWebApiTests.Models
{
    public class BonusStats
    {
        public int Stat { get; set; }
        public int Amount { get; set; }
    }
}
