﻿
namespace WowCommunityWebApiTests.Models
{
    public class ItemSet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SetBonuses[] SetBonuses { get; set; }
        public int[] Items { get; set; }
    }
}
