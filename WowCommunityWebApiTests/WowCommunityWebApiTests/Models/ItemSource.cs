﻿
namespace WowCommunityWebApiTests.Models
{
    public class ItemSource
    {
        public int SourceId { get; set; }
        public string SourceType { get; set; }
    }
}
