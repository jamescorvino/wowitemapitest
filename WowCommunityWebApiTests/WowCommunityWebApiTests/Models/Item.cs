﻿
namespace WowCommunityWebApiTests.Models
{
    public class Item
    {
        public int Id { get; set; }
        public int DisenchantingSkillRank { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public byte Stackgable { get; set; }
        public int[] AllowableClasses { get; set; }
        public byte ItemBind { get; set; }
        public BonusStats[] BonusStats { get; set; }
        // What is ItemSpells?
        public string[] ItemSpells { get; set; }
        public int BuyPrice { get; set; }
        public int ItemClass { get; set; }
        public int ItemSubClass { get; set; }
        public int ContainerSlots { get; set; }
        public WeaponInfo WeaponInfo { get; set; }
        public int InventoryType { get; set; }
        public bool Equippable { get; set; }
        public int ItemLevel { get; set; }
        public ItemSet ItemSet { get; set; }
        public int MaxCount { get; set; }
        public int MaxDurability { get; set; }
        public int MinFactionId { get; set; }
        public int MinRepuration { get; set; }
        public int Quality { get; set; }
        public int SellPrice { get; set; }
        public int RequiredSkill { get; set; }
        public int RequiredLevel { get; set; }
        public int RequiredSkillRank { get; set; }
        public SocketInfo SocketInfo { get; set; }
        public ItemSource ItemSource { get; set; }
        public int BaseArmor { get; set; }
        public bool HasSockets { get; set; }
        public bool IsAuctionable { get; set; }
        public int Armor { get; set; }
        public int DisplayInfoId { get; set; }
        public string NameDescription { get; set; }
        public int NameDescriptionColor { get; set; }
        public bool Upgradeable { get; set; }
        public bool HeroicTooltip { get; set; }
        public string Context { get; set; }
        // What is  BonusLists?
        public string[] BonusLists { get; set; }
        // What is  AvailableContexts lists?
        public string[] AvailableContexts { get; set; }
        // What is  BonusSummary?
        public BonusSummary BonusSummary { get; set; }
    }
}
