﻿
namespace WowCommunityWebApiTests.Models
{
    public class BonusSummary
    {
        public string[] DefaultBonusList { get; set; }
        public string[] ChanceBonusList { get; set; }
        public string[] BonusChances { get; set; }
    }
}
