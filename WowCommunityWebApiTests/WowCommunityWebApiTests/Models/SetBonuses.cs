﻿
namespace WowCommunityWebApiTests.Models
{
    public class SetBonuses
    {
        public string Description { get; set; }
        public int Threshold { get; set; }
    }
}
