﻿
using WowCommunityWebApiTests.Models;

namespace WowCommunityWebApiTests
{
    public class ItemApi : BaseApi<Item>
    {
        public ItemApi(int id) : base(id)
        {
            AccessIdentifier = "item";
        }

        public override sealed string AccessIdentifier { get; set; }
    }
}
